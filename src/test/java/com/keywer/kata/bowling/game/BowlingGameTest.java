package com.keywer.kata.bowling.game;

import com.keywer.kata.bowling.game.game.BowlingGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;


public class BowlingGameTest {

    private BowlingGame game;

    @BeforeEach
    private void setUp() {
        game = new BowlingGame();
    }

    @Test
    void should_return_zero_when_gutter() {
        //GIVEN
        int pins=0;
        //WHEN
        game.roll(pins);
        //THEN
        assertThat(game.score()).isEqualTo(0);
    }

    @Test
    void should_compute_a_score_when_launch_somes_classical_rolls() {
        //GIVEN
        int firstThrow =1;
        int secondThrow =1;
        int thirdThrow =3;
        int fourthThrow =1;
        //WHEN
        game.roll(firstThrow);
        game.roll(secondThrow);
        game.roll(thirdThrow);
        game.roll(fourthThrow);
        //THEN
        assertThat(game.score()).isEqualTo(6);
    }

    @Test
    void should_adding_twice_the_next_roll_score_when_doing_spare() {
        //GIVEN
        int firstThrow =6;
        int secondThrow =4;
        int thirdThrow =2;
        int fourthThrow =3;
        int fifthThrow =1;
        //WHEN
        game.roll(firstThrow);
        game.roll(secondThrow);
        game.roll(thirdThrow);
        game.roll(fourthThrow);
        game.roll(fifthThrow);
        //THEN
        assertThat(game.score()).isEqualTo(17);
    }

    @Test
    void should_adding_twice_the_two_next_rolls_score_when_doing_strike() {
        //GIVEN
        int firstThrow =10;
        int secondThrow =2;
        int thirdThrow =4;
        //WHEN
        game.roll(10);
        game.roll(2);
        game.roll(4);
        //THEN
        assertThat(game.score()).isEqualTo(22);
    }


    @Test
    void should_be_finish_when_twenty_rolls_when_there_are_only_classical_throws() {
        //GIVEN
        int classicalThrow=4;
        //WHEN
        repeatRoll(30, classicalThrow);
        //THEN
        assertThat(game.score()).isEqualTo(80);
    }


    @Test
    void should_allow_a_last_rolls_when_twenty_rolls_were_throw_with_an_ended_strike() {
        //GIVEN
        int classicalThrow=4;
        int strikeInLastFrame =10;
        int classicalThrowInLastFrame=8;
        int secondClassicalThrowInLastFrame=1;
        //WHEN
        repeatRoll(18, classicalThrow);
        game.roll(strikeInLastFrame);
        game.roll(classicalThrowInLastFrame);
        game.roll(secondClassicalThrowInLastFrame);
        //THEN
        assertThat(game.score()).isEqualTo(91);
    }

    private void repeatRoll(int number, int value) {
        IntStream.range(0,number).forEach(
                cpt->game.roll(value)
        );
    }
}
