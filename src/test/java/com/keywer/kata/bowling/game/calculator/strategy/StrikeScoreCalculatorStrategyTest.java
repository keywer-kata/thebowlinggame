package com.keywer.kata.bowling.game.calculator.strategy;

import com.keywer.kata.bowling.game.frame.type.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StrikeScoreCalculatorStrategyTest {

    @Test
    void should_compute_score(){
        //GIVEN
        Frame first = new StrikeFrame();
        Frame second = new NormalFrame(3,2);
        List<Frame> frames = List.of(first,second);
        //WHEN
        int score = new StrikeScoreCalculatorStrategy().computeFrameScore(frames, 0);
        //THEN
        assertThat(score).isEqualTo(15);
    }


    @Test
    void should_compute_score_with_multiple_strike(){
        //GIVEN
        Frame first = new StrikeFrame();
        Frame second = new StrikeFrame();
        Frame third = new NormalFrame(2,1);
        List<Frame> frames = List.of(first,second,third);
        //WHEN
        int score = new StrikeScoreCalculatorStrategy().computeFrameScore(frames, 0);
        //THEN
        assertThat(score).isEqualTo(22);
    }


    @Test
    void should_compute_score_with_triple_strike(){
        //GIVEN
        Frame first = new StrikeFrame();
        Frame second = new StrikeFrame();
        Frame third = new StrikeFrame();
        List<Frame> frames = List.of(first,second,third);
        //WHEN
        int score = new StrikeScoreCalculatorStrategy().computeFrameScore(frames, 0);
        //THEN
        assertThat(score).isEqualTo(30);
    }

    @Test
    void should_compute_score_with_strike_at_the_end_frame(){
        //GIVEN
        Frame first = new SpecialTenthFrame(10,5,4);
        List<Frame> frames = List.of(first);
        //WHEN
        int score = new StrikeScoreCalculatorStrategy().computeFrameScore(frames, 0);
        //THEN
        assertThat(score).isEqualTo(19);
    }


    @Test
    void should_compute_score_with_strike_following_by_a_spare(){
        //GIVEN
        Frame first = new StrikeFrame();
        Frame second = new SpareFrame(8,2);
        List<Frame> frames = List.of(first,second);
        //WHEN
        int score = new StrikeScoreCalculatorStrategy().computeFrameScore(frames, 0);
        //THEN
        assertThat(score).isEqualTo(20);
    }
}