package com.keywer.kata.bowling.game.calculator.strategy;

import com.keywer.kata.bowling.game.frame.type.Frame;
import com.keywer.kata.bowling.game.frame.type.NormalFrame;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

class NormalScoreCalculatorStrategyTest {

    @Test
    void should_compute_score_with_normal_frame(){
        // GIVEN
        Frame frame = new NormalFrame(2,6);
        //WHEN
        int score = new NormalScoreCalculatorStrategy().computeFrameScore(Collections.singletonList(frame), 0);
        //THEN
        assertThat(score).isEqualTo(8);
    }
}