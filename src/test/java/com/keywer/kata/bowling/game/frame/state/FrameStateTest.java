package com.keywer.kata.bowling.game.frame.state;

import com.keywer.kata.bowling.game.frame.type.*;
import com.keywer.kata.bowling.game.game.BowlingGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;


class FrameStateTest {

    private FrameState frameState;

    @BeforeEach
    private void setUp() {
        frameState = new FirstFrameState(null);
    }

    @Test
    void should_be_a_normal_state_when_launch_two_not_special_throw_and_not_last_frame() {
        //GIVEN
        boolean isLastFrame = false;
        int firstThrow = 5;
        int secondThrow = 4;
        //WHEN
        Optional<Frame> compute = frameState.compute(firstThrow, isLastFrame);
        frameState = frameState.next();
        compute = frameState.compute(secondThrow, isLastFrame);
        //THEN
        assertThat(compute.get()).isInstanceOf(NormalFrame.class);
    }

    @Test
    void should_be_a_spare_state_when_launch_two_throw_equals_to_ten_and_not_last_frame() {
        //GIVEN
        boolean isLastFrame = false;
        int firstThrow = 6;
        int secondThrow = 4;
        //WHEN
        Optional<Frame> compute = frameState.compute(firstThrow, isLastFrame);
        frameState = frameState.next();
        compute = frameState.compute(secondThrow, isLastFrame);
        //THEN
        assertThat(compute.get()).isInstanceOf(SpareFrame.class);
    }

    @Test
    void should_be_a_strike_state_when_launch_one_throw_equals_to_ten_and_not_last_frame() {
        //GIVEN
        boolean isLastFrame = false;
        int firstThrow = 10;
        //WHEN
        Optional<Frame> compute = frameState.compute(firstThrow, isLastFrame);
        //THEN
        assertThat(compute.get()).isInstanceOf(StrikeFrame.class);
    }

    @Test
    void should_be_a_SpecialTenthFrame_state_when_launch_one_throw_equals_to_ten_and_last_frame() {
        //GIVEN
        boolean isLastFrame = true;
        int firstThrow = 10;
        int secondThrow = 5;
        int thirdThrow = 6;
        //WHEN
        Optional<Frame> compute = frameState.compute(firstThrow, isLastFrame);
        frameState = frameState.next();
        compute = frameState.compute(secondThrow, isLastFrame);
        frameState = frameState.next();
        compute = frameState.compute(thirdThrow, isLastFrame);
        //THEN
        assertThat(compute.get()).isInstanceOf(SpecialTenthFrame.class);
    }

    @Test
    void should_be_a_normal_frame_state_when_launch_two_not_special_throw_and_penultimate() {
        //GIVEN
        boolean isLastFrame = true;
        int firstThrow = 10;
        int secondThrow = 1;
        int thirdThrow = 5;
        //WHEN
        Optional<Frame> compute = frameState.compute(firstThrow, false);
        frameState = frameState.next();
        compute = frameState.compute(secondThrow, isLastFrame);
        frameState = frameState.next();
        compute = frameState.compute(thirdThrow, isLastFrame);
        //THEN
        assertThat(compute.get()).isInstanceOf(NormalFrame.class);
    }

}