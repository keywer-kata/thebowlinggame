package com.keywer.kata.bowling.game.calculator.strategy;

import com.keywer.kata.bowling.game.frame.type.Frame;
import com.keywer.kata.bowling.game.frame.type.NormalFrame;
import com.keywer.kata.bowling.game.frame.type.SpareFrame;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SpareScoreCalculatorStrategyTest {

    @Test
    void should_compute_score_with_spare_frame(){
        //GIVEN
        Frame first = new SpareFrame(2,8);
        Frame second = new NormalFrame(3,2);
        List<Frame> frames = List.of(first,second);
        //WHEN
        int score = new SpareScoreCalculatorStrategy().computeFrameScore(frames, 0);
        //THEN
        assertThat(score).isEqualTo(13);
    }
}