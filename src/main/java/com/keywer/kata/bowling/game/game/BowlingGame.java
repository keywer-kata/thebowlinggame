package com.keywer.kata.bowling.game.game;

import com.keywer.kata.bowling.game.calculator.ScoreCalculator;
import com.keywer.kata.bowling.game.frame.state.FrameState;
import com.keywer.kata.bowling.game.frame.state.FinalFrameState;
import com.keywer.kata.bowling.game.frame.state.FirstFrameState;
import com.keywer.kata.bowling.game.frame.type.Frame;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class BowlingGame {

    private static int MAXIMUM_FRAME_NUMBER = 10;
    private int cptFrame = 0;
    private FrameState frameState = new FirstFrameState(null);
    private List<Frame> frames = new ArrayList();

    private ScoreCalculator scoreCalculator = new ScoreCalculator();

    public void roll(int pins) {
        if( !(frameState instanceof FinalFrameState)) {
            Optional<Frame> aFrame = frameState.compute(pins, cptFrame == MAXIMUM_FRAME_NUMBER -1);
            if (aFrame.isPresent()) {
                frames.add(aFrame.get());
                cptFrame++;
            }
            frameState = frameState.next();
        }
    }


    public int score() {
        return IntStream
                .range(0, frames.size())
                .map((cpt) -> scoreCalculator.computeFrameScore(frames, cpt))
                .reduce(0, (a, b) -> a + b);
    }

}
