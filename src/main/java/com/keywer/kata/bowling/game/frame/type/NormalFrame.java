package com.keywer.kata.bowling.game.frame.type;

import com.keywer.kata.bowling.game.calculator.strategy.NormalScoreCalculatorStrategy;
import com.keywer.kata.bowling.game.calculator.strategy.ScoreCalculatorStrategy;

public class NormalFrame implements Frame {

    private final int firstThrow;
    private final int secondThrow;

    public NormalFrame(int firstThrow, int secondThrow) {
        this.firstThrow = firstThrow;
        this.secondThrow = secondThrow;
    }

    public int getFirstThrow() {
        return firstThrow;
    }

    public int getSecondThrow() {
        return secondThrow;
    }


    @Override
    public ScoreCalculatorStrategy getScoreCalculatorStrategy() {
        return new NormalScoreCalculatorStrategy();
    }


}
