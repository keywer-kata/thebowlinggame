package com.keywer.kata.bowling.game.frame.type;

import com.keywer.kata.bowling.game.calculator.strategy.ScoreCalculatorStrategy;

public interface Frame {

    ScoreCalculatorStrategy getScoreCalculatorStrategy();

    int getFirstThrow();

}
