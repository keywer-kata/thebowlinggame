package com.keywer.kata.bowling.game.frame.state;

import com.keywer.kata.bowling.game.frame.type.NormalFrame;
import com.keywer.kata.bowling.game.frame.type.Frame;
import com.keywer.kata.bowling.game.frame.type.SpareFrame;

import java.util.Optional;

public class SecondFrameState implements FrameState {

    private Optional<Integer> pinsThow;
    private boolean isLastFrame = false;
    private FrameState prec;

    public SecondFrameState(FrameState prec) {
        this.prec = prec;
    }


    @Override
    public FrameState prec() {
        return prec;
    }

    @Override
    public FrameState next() {
        FrameState frameState = new FirstFrameState(this);
        if (isLastFrame) {
            if (checkSpecialRolls()) {
                frameState = new ThirdFrameState(this);
            }else{
                frameState = new FinalFrameState(this);
            }
        }
        return frameState;
    }

    @Override
    public Optional<Frame> compute(int pins, boolean isLastFrame) {
        pinsThow = Optional.of(pins);
        this.isLastFrame = isLastFrame;
        Optional frame = Optional.empty();

        if(this.prec.getPins() + pins == 10 && !isLastFrame) {
            frame = Optional.of(new SpareFrame(this.prec.getPins(), pins));
        }
        if(!isLastFrame && !frame.isPresent() || isLastFrame && !checkSpecialRolls()) {
            frame = Optional.of(new NormalFrame(this.prec.getPins(), pins));
        }

        return frame;
    }

    @Override
    public int getPins() {
        return this.pinsThow.get();
    }

    private boolean checkSpecialRolls(){
        return this.prec.getPins() == 10 || this.prec.getPins()+pinsThow.get() == 10 || pinsThow.filter(pins -> 10 == pins).isPresent();
    }
}
