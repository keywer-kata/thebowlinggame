package com.keywer.kata.bowling.game.calculator.strategy;

import com.keywer.kata.bowling.game.frame.type.Frame;

import java.util.List;

public interface ScoreCalculatorStrategy {
    int computeFrameScore(List<Frame> frames, int cpt);
}
