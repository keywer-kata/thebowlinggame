package com.keywer.kata.bowling.game.calculator.strategy;

import com.keywer.kata.bowling.game.frame.type.Frame;

import java.util.List;

public class SpareScoreCalculatorStrategy implements ScoreCalculatorStrategy {
    @Override
    public int computeFrameScore(List<Frame> frames, int cpt) {
        Frame current = frames.get(cpt);
        int score = 10;
        if (frames.size() > cpt + 1) {
            Frame next = frames.get(cpt + 1);
            score += next.getFirstThrow();
        }
        return score;
    }
}
