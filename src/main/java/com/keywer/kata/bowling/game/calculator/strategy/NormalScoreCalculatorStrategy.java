package com.keywer.kata.bowling.game.calculator.strategy;

import com.keywer.kata.bowling.game.frame.type.NormalFrame;
import com.keywer.kata.bowling.game.frame.type.Frame;

import java.util.List;

public class NormalScoreCalculatorStrategy implements ScoreCalculatorStrategy {
    @Override
    public int computeFrameScore(List<Frame> frames, int cpt) {
        NormalFrame current = (NormalFrame) frames.get(cpt);
        return current.getFirstThrow() + current.getSecondThrow();
    }
}
