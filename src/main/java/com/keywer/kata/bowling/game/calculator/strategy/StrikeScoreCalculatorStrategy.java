package com.keywer.kata.bowling.game.calculator.strategy;

import com.keywer.kata.bowling.game.frame.type.*;

import java.util.List;


public class StrikeScoreCalculatorStrategy implements ScoreCalculatorStrategy {
    @Override
    public int computeFrameScore(List<Frame> frames, int cpt) {
        int score = 10;
        Frame current = frames.get(cpt);
        if (current instanceof SpecialTenthFrame) {
            SpecialTenthFrame specialTenthFrame = (SpecialTenthFrame) current;
            score = specialTenthFrame.getFirstThrow() + specialTenthFrame.getSecondThrow() + specialTenthFrame.getThirdThrow();
        } else {
            if (frames.size() > cpt + 1) {
                Frame next = frames.get(cpt+1);
                score += next.getFirstThrow();
                if(next instanceof StrikeFrame && frames.size() > cpt + 2){
                    score += frames.get(cpt+2).getFirstThrow();
                }else{
                    if( next instanceof NormalFrame) {
                        score += ((NormalFrame) next).getSecondThrow();
                    }
                    if(next instanceof SpareFrame){
                        score += ((SpareFrame) next).getSecondThrow();
                    }
                }
            }
        }
        return score;
    }
}
