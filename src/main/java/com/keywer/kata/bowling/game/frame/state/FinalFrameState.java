package com.keywer.kata.bowling.game.frame.state;

import com.keywer.kata.bowling.game.frame.type.Frame;

import java.util.Optional;

public class FinalFrameState implements FrameState {

    public FinalFrameState(FrameState frameState) {
    }

    @Override
    public FrameState prec() {
        return null;
    }

    @Override
    public FrameState next() {
        return null;
    }

    @Override
    public Optional<Frame> compute(int pins, boolean isLastFrame) {
        return Optional.empty();
    }

    @Override
    public int getPins() {
        return 0;
    }
}
