package com.keywer.kata.bowling.game.frame.state;


import com.keywer.kata.bowling.game.frame.type.Frame;
import com.keywer.kata.bowling.game.frame.type.StrikeFrame;

import java.util.Optional;

public class FirstFrameState implements FrameState {

    private Optional<Integer> pinsThow;
    private boolean isLastFrame = false;
    private FrameState prec;

    public FirstFrameState(FrameState prec){
        this.prec=prec;
    }

    @Override
    public FrameState prec() {
        return prec;
    }

    @Override
    public FrameState next() {
        FrameState frameState = new SecondFrameState(this);
        if(pinsThow.filter(pins-> 10 == pins).isPresent() && !isLastFrame){
            frameState = new FirstFrameState(this);
        }
        return frameState;
    }

    @Override
    public Optional<Frame> compute(int pins, boolean isLastFrame) {
        pinsThow = Optional.of(pins);
        this.isLastFrame=isLastFrame;
        Optional frame = Optional.empty();
        if(!isLastFrame) {
            frame = pinsThow.filter(aPins -> 10 == aPins).map(aPins -> new StrikeFrame());
        }
        return frame;
    }

    @Override
    public int getPins() {
        return pinsThow.get();
    }
}
