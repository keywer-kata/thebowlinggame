package com.keywer.kata.bowling.game.frame.type;

import com.keywer.kata.bowling.game.calculator.strategy.ScoreCalculatorStrategy;
import com.keywer.kata.bowling.game.calculator.strategy.StrikeScoreCalculatorStrategy;

public class StrikeFrame implements Frame {

    @Override
    public ScoreCalculatorStrategy getScoreCalculatorStrategy() {
        return new StrikeScoreCalculatorStrategy();
    }

    @Override
    public int getFirstThrow() {
        return 10;
    }

}
