package com.keywer.kata.bowling.game.frame.type;

import com.keywer.kata.bowling.game.calculator.strategy.ScoreCalculatorStrategy;
import com.keywer.kata.bowling.game.calculator.strategy.SpareScoreCalculatorStrategy;

public class SpareFrame implements Frame {
    private final int firstThrow;
    private final int secondThrow;

    public SpareFrame(int firstThrow, int secondThrow) {
        if (firstThrow + secondThrow != 10) throw new IllegalArgumentException("There are not enought pins to create a SpareFrame");
        this.firstThrow = firstThrow;
        this.secondThrow = secondThrow;
    }


    @Override
    public ScoreCalculatorStrategy getScoreCalculatorStrategy() {
        return new SpareScoreCalculatorStrategy();
    }

    @Override
    public int getFirstThrow() {
        return firstThrow;
    }

    public int getSecondThrow() {
        return secondThrow;
    }

}
