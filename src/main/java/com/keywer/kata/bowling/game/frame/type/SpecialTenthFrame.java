package com.keywer.kata.bowling.game.frame.type;

import com.keywer.kata.bowling.game.calculator.strategy.ScoreCalculatorStrategy;
import com.keywer.kata.bowling.game.calculator.strategy.SpecialTenthScoreCalculator;

public class SpecialTenthFrame implements Frame {

    private final int firstThrow;
    private final int secondThrow;
    private Integer thirdThrow;

    public SpecialTenthFrame(int firstThrow, int secondThrow, int thirdThrow) {
        this.firstThrow = firstThrow;
        this.secondThrow = secondThrow;
        this.thirdThrow= thirdThrow;
    }

    @Override
    public ScoreCalculatorStrategy getScoreCalculatorStrategy() {
        return new SpecialTenthScoreCalculator();
    }

    @Override
    public int getFirstThrow() {
        return firstThrow;
    }

    public int getSecondThrow() {
        return secondThrow;
    }

    public Integer getThirdThrow() {
        return thirdThrow;
    }

}
