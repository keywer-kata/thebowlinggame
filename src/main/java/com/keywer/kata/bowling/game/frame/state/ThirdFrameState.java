package com.keywer.kata.bowling.game.frame.state;

import com.keywer.kata.bowling.game.frame.type.Frame;
import com.keywer.kata.bowling.game.frame.type.SpecialTenthFrame;

import java.util.Optional;

public class ThirdFrameState implements FrameState {
    private Optional<Integer> pinsThow;
    private FrameState prec;

    public ThirdFrameState(FrameState frameState) {
        this.prec = frameState;
    }

    @Override
    public FrameState prec() {
        return prec;
    }

    @Override
    public FrameState next() {
        return new FinalFrameState(this);
    }

    @Override
    public Optional<Frame> compute(int pins, boolean isLastFrame) {
        pinsThow = Optional.of(pins);
        return Optional.of(new SpecialTenthFrame(prec.prec().getPins(), prec.getPins(), pins));
    }

    @Override
    public int getPins() {
        return this.pinsThow.get();
    }
}
