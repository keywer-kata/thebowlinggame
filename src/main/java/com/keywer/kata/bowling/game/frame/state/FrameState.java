package com.keywer.kata.bowling.game.frame.state;

import com.keywer.kata.bowling.game.frame.type.Frame;

import java.util.Optional;

public interface FrameState {

    FrameState prec();

    FrameState next();

    Optional<Frame> compute(int pins, boolean isLastFrame);

    int getPins();
}
