package com.keywer.kata.bowling.game.calculator.strategy;

import com.keywer.kata.bowling.game.frame.type.Frame;
import com.keywer.kata.bowling.game.frame.type.SpecialTenthFrame;

import java.util.List;

public class SpecialTenthScoreCalculator implements ScoreCalculatorStrategy {
    @Override
    public int computeFrameScore(List<Frame> frames, int cpt) {
        SpecialTenthFrame current = (SpecialTenthFrame)frames.get(cpt);
        return current.getFirstThrow() + current.getSecondThrow() + current.getThirdThrow();
    }
}
