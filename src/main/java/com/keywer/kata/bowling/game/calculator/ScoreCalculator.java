package com.keywer.kata.bowling.game.calculator;

import com.keywer.kata.bowling.game.frame.type.Frame;

import java.util.List;

public class ScoreCalculator {

    public int computeFrameScore(List<Frame> rolls, int cpt) {
        return rolls.get(cpt)
                .getScoreCalculatorStrategy()
                .computeFrameScore(rolls,cpt);
    }
}
